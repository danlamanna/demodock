# demodock
An easy web demo server with Nginx and Docker

## Example Usage

In the ``share/example`` directory of this repository, you will find some
example applications to demonstrate how demodock is used. The directories you
find there are as follows:

* ``app/`` - A simple web application, bundled with a Dockerfile
* ``other-app`` - Another simple web application, also with a Dockerfile
* ``web/`` - A dynamic web application that will serve a list of active demos
  (in turn, managed by demodock)

To serve the two example applications under the "demo site" application, follow
these instructions.

1. **Put demodock in the path.**

	With a shell command like the following, make sure that the ``demodock``
executable is in your path:

    ````sh
	export PATH=/path/to/demodock/bin:$PATH
	````

2. **Initialize demodock.**

    If you haven't used demodock before, you must *initiliaze* it with the
following command

	````sh
	demodock init
	````

    This creates a configuration directory at ``~/.config/demodock`` containing
control information. Demodock is now ready to use.

3. **Build the application images.**

	We need to build docker images for the two example applications. Issue
the following commands to do so:

	````sh
	cd share/example/app
	demodock build lorem

	cd ../other-app
	demodock build bladerunner
	````

	This will cause two new docker images to be built, with names
``docker/lorem`` and ``docker/bladerunner``. If you want to know what exactly
these images will do when run, you can peek at each application's
``Dockerfile``. Because this example is meant to show how to go about using the
system in general, you will see that both applications are extremely simple and
straightforward.

4. **Start a demodock server instance.**

	We now want to start up demodock itself, before launching the example
applications. We want to do this using the ``web`` directory as the web root,
since that directory contains a simple webpage infrastructure that can
dynamically show which demos are running:

	````sh
	cd ../web
	demodock up -r . -p 8000
	````

	You can use any available port number for the ``-p`` argument, and the
``-r`` argument is optional in this case, since the web root will default to
the current directory when it is omitted.

	At this point, demodock is running. If you visit http://localhost:8000 now,
you'll see a basic page with no listed links to demos. The next steps will add
some.

5. **Run the applications as demodock demos.**

	Now we want to to run the images we built previously, via the following commands:

	````sh
	demodock start lorem alpha
	demodock start bladerunner beta
	````

	These commands launch the named image (*viz.* ``lorem``/``bladerunner``) in
containers named by the second argument (*viz.* ``alpha``/``beta``).

	(Strictly speaking, the container name is not simply the tag passed on the
command line; demodock appends a timestamp consisting of the date, time, and
nanosecond count to the requested tag. This is to minimize the risk of
conflicting names, even when they may be generated multiple times per second in
an extreme case.)

	You can verify that the containers are running by issuing this command:

	````sh
	demodock ps
	````

6. **Update the server.**

	Since new demos have been started, the server must be informed of the
change in the running demo list. The following command will perform the update
and print a JSON list of running applications:

	````sh
	demodock update
	````

	We actually want to use this list as input to the main demo site (housed in
``web``, and used as the web root in launching the demodock server in step 4
above). The following command will capture the output in a file where our site
expects to find it:

	````sh
	demodock update >demos.json
	````

	If you now visit http://localhost:8000 again, you should see a list of two
links, leading to our two demos!
