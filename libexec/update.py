import json
import sys


def main():
    config = json.loads(sys.stdin.read())

    running = []
    for data in config:
        if data.get("Config", {}).get("Env", {}) is None or \
           data.get("Config", {}).get("ExposedPorts") is None or \
           data.get("NetworkSettings", {}).get("IPAddress") is None or \
           data.get("State", {}).get("StartedAt") is None:
            continue

        env = {k: v for k, v in (x.split("=", 1) for x in data["Config"]["Env"] if "=" in x)}

        if "DEMODOCK_KEY" not in env:
            continue

        demo = {"id": data["Id"],
                "started": data["State"]["StartedAt"],
                "ip": data["NetworkSettings"]["IPAddress"],
                "ports": []}

        # To signal that a running container is ready to be mounted, it should
        # export a DEMODOCK_READY environment variable, set to any value.
        if env.get("DEMODOCK_READY") is None:
            continue

        # Install all transmitted environment variables in the demo info object.
        prefix = "DEMODOCK_"
        for k, v in env.iteritems():
            if k.startswith(prefix):
                key = k[len(prefix):].lower()
                if key:
                    demo[key] = v

        for key in (x for x in data["Config"]["ExposedPorts"] if x.endswith("/tcp")):
            try:
                port = int(key.split("/tcp")[0])
            except Exception:
                continue
            demo["ports"].append(port)

        if not demo["ports"]:
            continue

        running.append((demo["started"], demo))

    running = [item[1] for item in sorted(running)]

    keycount = {}
    proxies = []
    for demo in running:
        for port in demo["ports"]:
            key = demo["key"]
            if key not in keycount:
                keycount[key] = 1
            else:
                key = "%s-%d" % (key, keycount[key])
                keycount[demo["key"]] += 1
            proxies.append("""location /%s/ {
  proxy_set_header X-Forwarded-Host $http_host;
  proxy_set_header X-Forwarded-Server $host;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-Forwarded-Proto $scheme;
  proxy_pass http://%s:%d/;
}""" % (key, demo['ip'], port))

    print "\n".join(proxies)

    print >>sys.stderr, json.dumps(running, indent=4, separators=(",", ": "))


if __name__ == "__main__":
    sys.exit(main())
